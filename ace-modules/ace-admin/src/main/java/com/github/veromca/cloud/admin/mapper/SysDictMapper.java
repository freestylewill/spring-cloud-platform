package com.github.veromca.cloud.admin.mapper;

import com.github.veromca.cloud.admin.entity.SysDict;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-11-30 16:34:17
 */
public interface SysDictMapper extends Mapper<SysDict> {
	
}
