package com.github.veromca.cloud.admin.rest;

import com.github.veromca.cloud.admin.biz.SysDictBiz;
import com.github.veromca.cloud.admin.entity.SysDict;
import com.github.veromca.cloud.common.rest.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("sysDict")
public class SysDictController extends BaseController<SysDictBiz,SysDict> {

}