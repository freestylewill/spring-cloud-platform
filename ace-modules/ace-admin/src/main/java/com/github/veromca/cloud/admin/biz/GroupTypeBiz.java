package com.github.veromca.cloud.admin.biz;

import org.springframework.stereotype.Service;

import com.github.veromca.cloud.admin.entity.GroupType;
import com.github.veromca.cloud.admin.mapper.GroupTypeMapper;
import com.github.veromca.cloud.common.biz.BaseBiz;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-12 8:48
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GroupTypeBiz extends BaseBiz<GroupTypeMapper,GroupType> {
}
