package com.github.veromca.cloud.admin.mapper;

import com.github.veromca.cloud.admin.entity.GroupType;
import tk.mybatis.mapper.common.Mapper;

public interface GroupTypeMapper extends Mapper<GroupType> {
}