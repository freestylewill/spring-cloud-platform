package com.github.veromca.cloud.admin.mapper;

import com.github.veromca.cloud.admin.entity.GateLog;
import tk.mybatis.mapper.common.Mapper;

public interface GateLogMapper extends Mapper<GateLog> {
}