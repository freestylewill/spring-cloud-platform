package com.github.veromca.cloud.admin.biz;

import com.github.veromca.cloud.admin.entity.ResourceAuthority;
import com.github.veromca.cloud.admin.mapper.ResourceAuthorityMapper;
import com.github.veromca.cloud.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Ace on 2017/6/19.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ResourceAuthorityBiz extends BaseBiz<ResourceAuthorityMapper,ResourceAuthority> {
}
