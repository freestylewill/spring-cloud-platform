package com.github.veromca.cloud.admin.biz;

import org.springframework.stereotype.Service;

import com.github.veromca.cloud.admin.entity.SysDict;
import com.github.veromca.cloud.admin.mapper.SysDictMapper;
import com.github.veromca.cloud.common.biz.BaseBiz;

/**
 * 
 *
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-11-30 16:34:17
 */
@Service
public class SysDictBiz extends BaseBiz<SysDictMapper,SysDict> {
}