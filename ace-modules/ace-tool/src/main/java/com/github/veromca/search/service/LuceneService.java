

package com.github.veromca.search.service;


import com.github.veromca.cloud.api.vo.search.IndexObject;
import com.github.veromca.cloud.common.msg.TableResultResponse;

/**
 * lucense 接口
 * @author ace
 */
public interface LuceneService {

    void save(IndexObject indexObject);

    void update(IndexObject indexObject);

    void delete(IndexObject indexObject);

    void deleteAll();

    TableResultResponse page(Integer pageNumber, Integer pageSize, String keyword);
}
