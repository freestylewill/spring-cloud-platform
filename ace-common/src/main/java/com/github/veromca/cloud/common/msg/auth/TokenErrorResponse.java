package com.github.veromca.cloud.common.msg.auth;

import com.github.veromca.cloud.common.constant.RestCodeConstants;
import com.github.veromca.cloud.common.msg.BaseResponse;

/**
 * Created by ace on 2017/8/23.
 */
public class TokenErrorResponse extends BaseResponse {
    public TokenErrorResponse(String message) {
        super(RestCodeConstants.TOKEN_ERROR_CODE, message);
    }
}
