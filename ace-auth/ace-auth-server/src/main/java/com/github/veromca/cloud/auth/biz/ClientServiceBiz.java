package com.github.veromca.cloud.auth.biz;

import com.github.veromca.cloud.auth.entity.ClientService;
import com.github.veromca.cloud.auth.mapper.ClientServiceMapper;
import com.github.veromca.cloud.common.biz.BaseBiz;
import org.springframework.stereotype.Service;

/**
 * @author ace
 * @create 2017/12/30.
 */
@Service
public class ClientServiceBiz extends BaseBiz<ClientServiceMapper,ClientService> {
}
